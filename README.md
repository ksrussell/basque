# ARTHIST190T Final Project

###### Kyan Russell

## Results:

![demo](output.gif)

(slightly sped up)

## Build Instructions

My app is compressed in [basque.zip]. After unzipping the file, please build basque/App/Unity-iPhone.xcodeproj in XCode. Apple's ARKit requires that the user device conforms to the following [standards]. The app will not work otherwise. I have tested to make sure that it builds properly, but if it does not I hope the video demo will be sufficient.

[basque.zip]: https://bitbucket.org/ksrussell/basque/src/master/basque.zip

[standards]: http://www.redmondpie.com/ios-11-arkit-compatibility-check-if-your-device-is-compatible-with-apples-new-ar-platform/



## Proposal: 

My original proposal was to create a virtual reality sandbox along the lines of "The Powder Toy," a popular online game that recreates various materials and their corresponding physics in a 2D sandbox.

![powder toy](https://i.imgur.com/vx8Ym.gif)

However, after some testing it quickly became apparent that the amount of processing power needed to render several thousand particles, each with their own physics colliders, was not available on my phone. I was also unable to find a workaround. Thus, I decided to change tack and create a basketball game along the lines of Facebook Messenger's basketball game.

![basketball](http://ksassets.timeincuk.net/wp/uploads/sites/54/2016/03/ball-1.gif)

What I envisioned was a game where the user would have to swipe a virtual basketball into a virtual hoop placed somewhere in the real world. The user would be able to move around in the real world, while the virtual hoop remains static. Thus the user could move closer or further away from the hoop to change the difficulty of making a shot.

## Implementation: 

I coded this project mainly in Unity, using the [Unity ARKit Plugin]. Apple provides a developer kit called ARKit that allows iOS developers to easily interface with the augmented reality API. Unity's plugin allows developers to access ARKit through Unity, making it simple to create AR scenes and build straight to XCode.

I encountered several issues regarding how to best interface with the augmented reality scene, such as the following:

* Adding a forward vector so that the basketball travels away from the camera would not rotate when the user rotated the camera. I solved this by modifying the definition of "forward" to be relative to the camera, instead of a fixed direction in the world scene.

* Forcing the hoop to be in a fixed location. At first it appeared that the hoop would move with the camera, making it impossible to get closer to or further from it. I solved this after realizing that the hoop was in fact stationary, but was so far away from the camera that it appeared to move as well. I solved this by making it smaller and closer to the camera.

[Unity ARKit Plugin]: https://assetstore.unity.com/packages/essentials/tutorial-projects/unity-arkit-plugin-92515


## Closing Thoughts:

This was a fun project that tested how quickly I could learn how to use Unity, since I only had experience with ARKit. There were many interesting issues for which I had to find workarounds. If I were to extend this app, I would want to improve the actual game experience, since as of now it is merely a proof-of-concept. I would add limited attempts to score points, as well as different point values for different distances (3-pointers, etc). Finally, I would also have the hoop respawn in different locations in the world scene to add more interaction with the real world.







